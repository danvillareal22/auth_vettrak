Instructions for installing Vettrak Authetication on Moodle:

================================================================================



Step 1: Install the plugin (use master branch)

You can do this by either downloading the plugin and uploading it into the 'Auth' folder inside your Moodle site or using CLI to clone this git repo into the 'Auth' folder.




Step 2: Upgrade the site

After you install the plugin you will need to upgrade the site. This can be done through the frontend by navitgating to the 'notifications' under site administration or running 'php admin/cli/upgrade.php' from the moodle root code directory using CLI.




Step 3: Configure the plugin

There are four things that the must be provided by Vettrak (or the client) before you can configure the plugin:

Webservice URL
Username
System Client Code
Password

Once you have these credentials navigate to 'Site adminisitration -> Plugins -> Authetication -> Vettrak Authenticiation'.

After you have entered the credentials, configure the plugin as required and click save changes.




Step 4: Check the sync is running

Navigate to 'Site administration -> Server -> Scheduled tasks' and look for \auth_vettrak\task\syncUsers.

This will tell you if it has run, when it is going to run and how often it will run. 

For testing purposes you can run the scheduled task from CLI using the following command from the root of the Moodle code:

'php admin/tool/task/cli/schedule_task.php --execute=\\auth_vettrak\\task\\syncUsers'

This will also give you an error log.
