<?php

$tasks = array(                                                                                                                     
    array(                                                                                                                          
        'classname' => 'auth_vettrak\task\syncUsers',                                                                            
        'blocking' => 0,                                                                                                            
        'minute' => '24',                                                                                                            
        'hour' => '2',                                                                                                              
        'day' => '*',                                                                                                               
        'dayofweek' => '0',                                                                                                         
        'month' => '*'                                                                                                              
    )
);