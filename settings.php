<?php

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    $settings->add(new admin_setting_configtext('auth_vettrak/webservice', 'Webservice URL','', ''));
    $settings->add(new admin_setting_configtext('auth_vettrak/username', 'Username','', ''));
    $settings->add(new admin_setting_configtext('auth_vettrak/systemclientcode', 'System Client Code','', ''));
    $settings->add(new admin_setting_configpasswordunmask('auth_vettrak/password', 'Password','', ''));
    $settings->add(new admin_setting_configcheckbox('auth_vettrak/allow_periodic_user_sync', 'Enable Periodic User Synchronisation','',1));

    $settings->add(new admin_setting_configcheckbox('auth_vettrak/user_internal_password_management', 'User Internal Password Management','',0));

    $settings->add(new admin_setting_configcheckbox('auth_vettrak/user_synchronisation_use_accessible', 'Enable to configure User Synchronisation to use GetAccessibleClients rather than QueryAdditionalData','',1));

    $settings->add(new admin_setting_configtext('auth_vettrak/user_internal_password_default', 'Default Internal Password (Only available if Internal Password Management enabled)','', 'Learning-1'));
    
    $settings->add(new admin_setting_configtext('auth_vettrak/getaccessiblesincedate', 'Get Accessible Client - Since Date (Must be compatible with PHP DateTime Constructor)','', '1990'));

    $settings->add(new admin_setting_configtext('auth_vettrak/defaultemailifnotset', 'Default Client Email if none set','', 'unknownvettrak@logging.ecreators.com.au'));

    $settings->add(new admin_setting_configcheckbox('auth_vettrak/user_synchronisation_matchusernameoridnumber', 'Match User Synchronisation by Username OR idnumber column.','', 0));

    $settings->add(new admin_setting_configcheckbox('auth_vettrak/user_synchronisation_usernameisexternaldebtorcode', 'Use External Debtor Code as Username instead of Client Code','', 0));

    $settings->add(new admin_setting_configcheckbox('auth_vettrak/ignore_vettrak_zero_padding', 'Remove Client Code Zero Padding','', 0));

    $settings->add(new admin_setting_configcheckbox('auth_vettrak/fetch_profile_on_authentication', 'Fetch Updated Profile information on Authentication','', 0));

}