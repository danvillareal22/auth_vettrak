<?php

namespace auth_vettrak\task;

class syncUsers extends \core\task\scheduled_task {      
    public function get_name() {
        // Shown in admin screens
        return 'Synchronise Vettrak User Accounts';
    }
                                                                     
    public function execute() {       
        $vettrakauth = get_auth_plugin('vettrak');
		$vettrakauth->sync_users();
    }                                                                                                                               
} 