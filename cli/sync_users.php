<?php

ini_set('memory_limit','512M');

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');

set_debugging(DEBUG_DEVELOPER, true);

if (!is_enabled_auth('vettrak')) {
    error_log('[AUTH vettrak] '. 'vettrak plugin is not enabled.');
    die;
}

$vettrakauth = get_auth_plugin('vettrak');

$vettrakauth->sync_users();