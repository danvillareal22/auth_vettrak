<?php

define('CLI_SCRIPT', true);

require((dirname(dirname(dirname(__FILE__)))).'/config.php');

set_debugging(DEBUG_DEVELOPER, true);

if (!is_enabled_auth('vettrak')) {
    print_r('[AUTH vettrak] '. 'vettrak plugin is not enabled.');
    die;
}

$vettrakauth = get_auth_plugin('vettrak');

$subject = auth_plugin_vettrak::vettrak_webservice();
$pattern = '/^http.+?WSDL$/';
$matches = array();
$validURL = preg_match($pattern, $subject, $matches);
$endpointAccessible = @fopen($subject,"r");
$endpointAccessibleMessage = 'Web Service Endpoint is not accessible!';
if ($validURL) {
	$ValidURLmessage = 'Web Service URL is valid';
	if ($endpointAccessible) {
		$endpointAccessibleMessage = 'Web Service Endpoint is accessible!';
	}
} else {
	$ValidURLmessage = 'Web Service URL is not valid';
}

$client = $vettrakauth->vettrak_client();

$authentication = $client->call('ValidateClient', array(
    'sUsername' => auth_plugin_vettrak::vettrak_username(),
    'sPassword' => auth_plugin_vettrak::vettrak_password()
));

$GetPriorEducationList = $client->call('GetPriorEducationList');
$GetPriorEducationRecognitionList = $client->call('GetPriorEducationRecognitionList');
$GetSchoolLevelList = $client->call('GetSchoolLevelList');
$GetDisabilityList = $client->call('GetDisabilityList');

$GetStateList = $client->call('GetStateList');

$token = $vettrakauth->test_connection();

$GetStudyReasonsForState = $client->call('GetStudyReasonsForState', array(
  'stateShortName' => 'Vic',
));

$GetEmployerTypeList = $client->call('GetEmployerTypeList', array(
  'sToken' => $token
));

$GetEmploymentCategoryList = $client->call('GetEmploymentCategoryList');

$tmpClient = new SoapClient(auth_plugin_vettrak::vettrak_webservice());

?>
<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<h1>Vettrak Connection Test Page</h1>
<h2>Configuration</h2>
<dl>
  <dt>Web Service URL</dt>
  <dd <?php if ($validURL) { echo 'class="text-success"'; } else { echo 'class="text-danger"'; } ?>><?php var_dump($subject); ?></dd>
  <dt>Web Service URL Check</dt>
  <dd <?php if ($validURL) { echo 'class="text-success"'; } else { echo 'class="text-danger"'; } ?>><?php echo $ValidURLmessage; ?></dd>
  <dt>Web Service URL Accessible</dt>
  <dd <?php if ($endpointAccessible) { echo 'class="text-success"'; } else { echo 'class="text-danger"'; } ?>><?php echo $endpointAccessibleMessage; ?></dd>
  <dt>Username</dt>
  <dd <?php if (@auth_plugin_vettrak::vettrak_username()) { echo 'class="text-success"'; } else { echo 'class="text-danger"'; } ?>><?php var_dump(auth_plugin_vettrak::vettrak_username()); ?></dd>
  <dt>Password</dt>
  <dd <?php if (@auth_plugin_vettrak::vettrak_password()) { echo 'class="text-success"'; } else { echo 'class="text-danger"'; } ?>><?php var_dump(auth_plugin_vettrak::vettrak_password()); ?></dd>
  <dt>ValidateClientResponse</dt>
  <dd><pre><?php print_r($authentication); ?></pre></dd>
  <dt>Access Token</dt>
  <dd <?php if ($token) { echo 'class="text-success"'; } else { echo 'class="text-danger"'; } ?>><?php var_dump($token); ?></dd>
</dl>

<h2>GetPriorEducationList</h2>
<pre><?php print_r($GetPriorEducationList); ?></pre>

<h2>GetPriorEducationRecognitionList</h2>
<pre><?php print_r($GetPriorEducationRecognitionList); ?></pre>

<h2>GetSchoolLevelList</h2>
<pre><?php print_r($GetSchoolLevelList); ?></pre>

<h2>GetDisabilityList</h2>
<pre><?php print_r($GetDisabilityList); ?></pre>

<h2>GetStateList</h2>
<pre><?php print_r($GetStateList); ?></pre>

<h2>GetStudyReasonsForState</h2>
<pre><?php print_r($GetStudyReasonsForState); ?></pre>

<h2>GetEmployerTypeList</h2>
<pre><?php print_r($GetEmployerTypeList); ?></pre>

<h2>GetEmploymentCategoryList</h2>
<pre><?php print_r($GetEmploymentCategoryList); ?></pre>

